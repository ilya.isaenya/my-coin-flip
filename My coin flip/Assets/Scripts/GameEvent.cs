﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameEvent : MonoBehaviour
{
    public BackendManager bm;
    private CoinEvent ce;
    public Text[] winRounds;
    public GameObject resultPanel;
    public Slider slider;
    public GameObject obj;
    public GameObject panel;
    public GameObject btnStartGame;
    public Button[] btns;
    public Text balance;
    public Text balanceSliderValue;
    public Text resultText;
    private int roundCount = -1;
    private string endPointURL;
    public void StartGame()
    {
        resultText.text = "";
        resultPanel.SetActive(false);
        WWWForm form = new WWWForm();

        endPointURL = "users/" + bm.user.id.ToString() + "/games/start_game/";

        bm.Send(RequestType.Post, endPointURL, form, OnStartGame, bm.user.token);

        panel.SetActive(true);
        btnStartGame.SetActive(false);
    }
    public void OnStartGame(ResponseType responseType, string jsonResponse)
    {
        if (responseType != ResponseType.Success || jsonResponse == "") { Debug.Log(responseType); return; }

        bm.user.game = JsonUtility.FromJson<Game>(jsonResponse);
    }
    public void PlayRound(int choice)
    {
        WWWForm form = new WWWForm();
        form.AddField("bet", Convert.ToInt32(slider.value));
        form.AddField("choice", choice);

        endPointURL = "users/" + bm.user.id + "/games/" + bm.user.game.id + "/start_round/";

        bm.Send(RequestType.Post, endPointURL, form, OnPLayRound, bm.user.token);
    }
    public void OnPLayRound(ResponseType responseType, string jsonResponse)
    {
        if (responseType != ResponseType.Success || jsonResponse == "") { Debug.Log(responseType); return; }

        roundCount++;
        if (roundCount < 4)
            bm.user.round = JsonUtility.FromJson<Round>(jsonResponse);
        else
        {
            bm.user = JsonUtility.FromJson<User>(jsonResponse);
        }

        ce.Flip(bm.user.round.result);

        StartCoroutine(Wait());
    }
    public void Update()
    {
        balanceSliderValue.text = slider.value.ToString();
    }
    private void Awake()
    {
        ce = obj.GetComponent<CoinEvent>();
        bm.user.SetFromPrefs();
        slider.maxValue = bm.user.balance.balance;
        balance.text = bm.user.balance.balance.ToString();
    }
    public IEnumerator Wait()
    {
        btns[0].interactable = false;
        btns[1].interactable = false;

        yield return new WaitForSeconds(6);

        bm.user.SetBet(bm.user.round.bet, bm.user.round.result, bm.user.round.choice);
        balance.text = bm.user.balance.balance.ToString();
        slider.maxValue = bm.user.balance.balance;
        if (bm.user.round.result == bm.user.round.choice)
            winRounds[roundCount].text = "W";
        else
            winRounds[roundCount].text = "L";
        if (roundCount == 4)
        {
            yield return new WaitForSeconds(2);
            resultText.text = bm.user.game.result == 1 ? "YOU WIN" : "YOU LOSE";
            resultPanel.SetActive(true);
            for (int i = 0; i < 5; i++)
                winRounds[i].text = "";
            slider.maxValue = bm.user.balance.balance;
            balance.text = bm.user.balance.balance.ToString();
            roundCount = -1;
        }
        btns[0].interactable = true;
        btns[1].interactable = true;
    }
}
