﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public enum ResponseType
{
    ClientError, PageNotFound, RequestError, ParseError, Success
}

public enum RequestType
{
    Get, Post, Put, Delete
}

public class BackendManager : MonoBehaviour
{
    public User user = new User();
    private const string BaseURL = "http://127.0.0.1:8000/";
    public delegate void RequestResponseDelegate(ResponseType responseType, string jsonResponse);
    public void Send(RequestType type, string command, WWWForm wwwForm, RequestResponseDelegate onResponse = null, string authToken = "")
    {
        UnityWebRequest request = new UnityWebRequest();
        string url = BaseURL + command;

        wwwForm.headers.Add("Accept", "application/json");
        wwwForm.headers.Add("X-UNITY-METHOD", type.ToString().ToUpper());

        switch (type)
        {
            case RequestType.Post:
                request = UnityWebRequest.Post(url, wwwForm);
                break;
            case RequestType.Get:
                request = UnityWebRequest.Get(url);
                break;
        }
        if (authToken != "")
        {
            request.SetRequestHeader("Authorization", "Token " + authToken);
        }
        StartCoroutine(HandleRequest(request, onResponse));
    }
    private IEnumerator HandleRequest(UnityWebRequest request, RequestResponseDelegate onResponse)
    {
        using (UnityWebRequest webRequest = request)
        {
            yield return webRequest.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                onResponse(ResponseType.Success, webRequest.downloadHandler.text);
            }
        }
    }
}
