﻿using System;
using UnityEngine;

[Serializable]
public class User
{
    public int id;
    public string username;
    public string email;
    public string password;
    public string token;
    public Balance balance;
    public Game game;
    public Round round;
    public void Set(int id = 0, string username = "", string email = "", string password = "", string token = "", int balance_id = 0, int balance = 0, int last_balance = 0, int new_balance = 0)
    {
        if (id != 0)
        {
            this.id = id;
        }
        if (username != "")
        {
            this.username = username;
        }
        if (email != "")
        {
            this.email = email;
        }
        if (password != "")
        {
            this.password = password;
        }
        if (token != "")
        {
            this.token = token;
        }
        if (balance_id != 0)
        {
            this.balance.id = balance_id;
        }
        if (balance != 0)
        {
            this.balance.balance = balance;
        }
        if (last_balance != 0)
        {
            this.balance.last_balance = last_balance;
        }
        if (new_balance != 0)
        {
            this.balance.new_balance = new_balance;
        }
    }
    public void SetPrefs()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("id", this.id);
        PlayerPrefs.SetString("username", this.username);
        PlayerPrefs.SetString("email", this.email);
        PlayerPrefs.SetString("password", this.password);
        PlayerPrefs.SetString("token", this.token);
        PlayerPrefs.SetInt("balance_id", this.balance.id);
        PlayerPrefs.SetInt("balance_balance", this.balance.balance);
        PlayerPrefs.SetInt("balance_last_balance", this.balance.last_balance);
        PlayerPrefs.SetInt("balance_new_balance", this.balance.new_balance);
        PlayerPrefs.Save();
    }
    public void SetFromPrefs()
    {
        this.id = PlayerPrefs.GetInt("id");
        this.username = PlayerPrefs.GetString("username");
        this.email = PlayerPrefs.GetString("email");
        this.password = PlayerPrefs.GetString("password");
        this.token = PlayerPrefs.GetString("token");
        this.balance.id = PlayerPrefs.GetInt("balance_id");
        this.balance.balance = PlayerPrefs.GetInt("balance_balance");
        this.balance.last_balance = PlayerPrefs.GetInt("balance_last_balance");
        this.balance.new_balance = PlayerPrefs.GetInt("balance_new_balance");
    }
    public void SetBet(int bet, int result, int choice)
    {
        this.balance.last_balance = this.balance.balance;
        if (result == choice)
        {
            this.balance.balance += bet;
        }
        else
        {
            this.balance.balance -= bet;
        }
    }
}

[Serializable]
public class Game
{
    public int id;
    public int bank;
    public int result;
    public void Set(int id, int bank, int result)
    {
        this.id = id;
        this.bank = bank;
        this.result = result;
    }
}

[Serializable]
public class Round
{
    public int id;
    public int choice;
    public int result;
    public int bet;
}

[Serializable]
public class Balance
{
    public int id;
    public int balance;
    public int last_balance;
    public int new_balance;
}