﻿using System.Collections;
using UnityEngine;

public class CoinEvent : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    public Sprite[] sides;
    int flipCount;
    public void Flip(int result)
    {
        StartCoroutine(StartFlip(result));
    }
    private IEnumerator StartFlip(int result)
    {
        flipCount = 0;
        int count = 1;
        while (count <= 21 + result)
        {
            spriteRenderer.sprite = sides[flipCount % 2];
            yield return new WaitForSeconds(count * 0.025f);
            count++;
            flipCount++;
        }
    }
    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
}
