﻿using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginEvent : MonoBehaviour
{
    public BackendManager bm;
    public InputField input;
    public void Login()
    {
        if (!IsValidEmail(input.text))
        {
            Debug.Log("Email input error!");
            return;
        }
        else
        {
            WWWForm form = new WWWForm();
            form.AddField("username", input.text.Split(new char[] { '@' })[0]);
            form.AddField("email", input.text);
            form.AddField("password", input.text.Split(new char[] { '@' })[0]);
            PlayerPrefs.SetString("password", input.text.Split(new char[] { '@' })[0]);

            bm.Send(RequestType.Post, "api/users/", form, OnLogin);
        }
    }
    public void OnLogin(ResponseType responseType, string jsonResponse)
    {
        if (responseType != ResponseType.Success || jsonResponse == "")
        {
            Debug.Log(responseType);
            return;
        }
        bm.user = JsonUtility.FromJson<User>(jsonResponse);
        bm.user.SetPrefs();
        SceneManager.LoadScene("GameScene");
    }
    bool IsValidEmail(string email)
    {
        Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        Match match = regex.Match(email);
        return match.Success;
    }
}
